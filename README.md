# KiCad parts

These are custom symbols, footprints and models for the KiCad EDA suite.

## Installation

### Configuration

Copy the symbol and footprint library tables from `config/` to
`~/.config/kicad/` and set the variable `PARTS` to the path of this repository.

## 3D models

### Exporting from Blender

1. Export VRML2
2. Selection Only, Apply Modifiers, Texture/UVs, Vertex Colors<br/>
   Color: Vertex Color<br/>
   Forward: Y Forward<br/>
   Up: Z Up<br/>
   Scale: 1/2.54 = 0.393701<br/>
   Path Mode: Auto

### VRML2 Materials

Epoxy black:

```
ambientIntensity 0.293
diffuseColor 0.148 0.145 0.145
specularColor 0.180 0.168 0.160
emissiveColor 0.0 0.0 0.0
shininess 0.35
transparency 0.0
```

Plastic black rough:

```
ambientIntensity 0.314
diffuseColor 0.016 0.015 0.014
specularColor 0.388 0.384 0.382
emissiveColor 0.0 0.0 0.0
shininess 0.2
transparency 0.0
```

Plastic black shiny:

```
ambientIntensity 0.314
diffuseColor 0.016 0.015 0.014
specularColor 0.791 0.784 0.770
emissiveColor 0.0 0.0 0.0
shininess 0.6
transparency 0.0
```

Plastic white:

```
ambientIntensity 0.494
diffuseColor 0.895 0.891 0.813
specularColor 0.047 0.055 0.109
emissiveColor 0.0 0.0 0.0
shininess 0.25
transparency 0.0
```

Plastic green:

```
ambientIntensity 0.315
diffuseColor 0.340 0.680 0.445
specularColor 0.176 0.105 0.195
emissiveColor 0.0 0.0 0.0
shininess 0.25
transparency 0.0
```

Plastic blue:

```
ambientIntensity 0.565
diffuseColor 0.137 0.402 0.727
specularColor 0.359 0.379 0.270
emissiveColor 0.0 0.0 0.0
shininess 0.25
transparency 0.0
```

Plastic red:

```
ambientIntensity 0.565
diffuseColor 0.727 0.402 0.137
specularColor 0.270 0.379 0.359
emissiveColor 0.0 0.0 0.0
shininess 0.25
transparency 0.0
```

IC label:

```
ambientIntensity 0.082
diffuseColor 0.691 0.664 0.598
specularColor 0.000 0.000 0.000
emissiveColor 0.0 0.0 0.0
shininess 0.01
transparency 0.0
```

Ceramic white:

```
ambientIntensity 0.179
diffuseColor 0.995 0.991 0.913
specularColor 0.103 0.088 0.076
emissiveColor 0.0 0.0 0.0
shininess 0.1
transparency 0.0
```

Ceramic yellow:

```
ambientIntensity 0.579
diffuseColor 0.469 0.363 0.086
specularColor 0.328 0.258 0.172
emissiveColor 0.0 0.0 0.0
shininess 0.3
transparency 0.0
```

Ceramic gray:

```
ambientIntensity 0.179
diffuseColor 0.669 0.674 0.694
specularColor 0.103 0.088 0.076
emissiveColor 0.0 0.0 0.0
shininess 0.3
transparency 0.0
```

Ceramic black:

```
ambientIntensity 0.179
diffuseColor 0.273 0.273 0.273
specularColor 0.203 0.188 0.176
emissiveColor 0.0 0.0 0.0
shininess 0.15
transparency 0.0
```

Iron powder grey:

```
ambientIntensity 0.179
diffuseColor 0.573 0.575 0.579
specularColor 0.293 0.298 0.309
emissiveColor 0.0 0.0 0.0
shininess 0.25
transparency 0.0
```

Coating black:

```
ambientIntensity 0.638
diffuseColor 0.082 0.086 0.094
specularColor 0.066 0.063 0.063
emissiveColor 0.000 0.000 0.000
shininess 0.3
transparency 0.0
```

Coating orange:

```
ambientIntensity 0.512
diffuseColor 0.885 0.232 0.020
specularColor 0.075 0.104 0.102
emissiveColor 0.000 0.000 0.000
shininess 0.3
transparency 0.0
```

Coating green:

```
ambientIntensity 0.512
diffuseColor 0.234 0.332 0.180
specularColor 0.105 0.074 0.102
emissiveColor 0.000 0.000 0.000
shininess 0.3
transparency 0.0
```

Coating blue:

```
ambientIntensity 0.351
diffuseColor 0.000 0.334 0.785
specularColor 0.789 0.750 0.703
emissiveColor 0.000 0.000 0.000
shininess 0.3
transparency 0.0
```

Contact pins tin:

```
ambientIntensity 0.271
diffuseColor 0.824 0.820 0.781
specularColor 0.328 0.258 0.172
emissiveColor 0.0 0.0 0.0
shininess 0.70
transparency 0.0
```

Contact pins gold:

```
ambientIntensity 0.379
diffuseColor 0.859 0.738 0.496
specularColor 0.137 0.145 0.184
emissiveColor 0.0 0.0 0.0
shininess 0.40
transparency 0.0
```

Glass colorless:

```
ambientIntensity 0.789
diffuseColor 0.900 0.900 0.900
emissiveColor 0.000 0.000 0.000
specularColor 0.300 0.400 0.150
shininess 0.125
transparency 0.25
```

## License

Licensed under [Creative Commons Attribution-ShareAlike 4.0 International (CC
BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).
